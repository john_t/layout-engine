use crate::{layout::Vec2, Layout, MinimumNatural};
use either::{Either, Left, Right};

impl<L: Layout, R: Layout> Layout for Either<L, R> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        match self {
            Left(x) => x.width_for_height(height),
            Right(x) => x.width_for_height(height),
        }
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        match self {
            Left(x) => x.height_for_width(width),
            Right(x) => x.height_for_width(width),
        }
    }
    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        match self {
            Left(x) => x.prefered_size(),
            Right(x) => x.prefered_size(),
        }
    }
    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        match self {
            Left(x) => x.prefered_size_of_container(container),
            Right(x) => x.prefered_size_of_container(container),
        }
    }
}
