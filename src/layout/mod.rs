mod rect;
mod trbl;
mod vec2;
pub use rect::Rect;
pub use trbl::TRBL;
pub use vec2::Vec2;

/// A struct used to represent the space an item takes up on an axis.
///
/// This is mainly used with align.
#[derive(Debug, Clone, Copy, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct LayoutInfo {
    pub start: usize,
    pub end: usize,
}
