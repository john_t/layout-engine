use std::iter::Sum;
use std::ops::{Add, AddAssign, Sub, SubAssign};

use super::Vec2;

/// A struct to represent `top`, `right`, `bottom` and `left`.
///
/// This isn't used inside `layout_engine`
#[derive(Debug, Clone, Copy, PartialEq, Hash, Default)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct TRBL {
    pub top: usize,
    pub right: usize,
    pub bottom: usize,
    pub left: usize,
}

impl TRBL {
    pub const fn new(
        top: usize,
        right: usize,
        bottom: usize,
        left: usize,
    ) -> Self {
        TRBL {
            top,
            right,
            bottom,
            left,
        }
    }
}

impl TRBL {
    pub const fn vertical(&self) -> usize {
        self.top + self.bottom
    }

    pub const fn horizontal(&self) -> usize {
        self.left + self.right
    }

    pub const fn extra(&self) -> Vec2 {
        Vec2::new(self.horizontal(), self.vertical())
    }
}

impl Add for TRBL {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            top: self.top + other.top,
            right: self.right + other.right,
            bottom: self.bottom + other.bottom,
            left: self.left + other.left,
        }
    }
}

impl AddAssign for TRBL {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            top: self.top + other.top,
            right: self.right + other.right,
            bottom: self.bottom + other.bottom,
            left: self.left + other.left,
        };
    }
}

impl Sub for TRBL {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Self {
            top: self.top - other.top,
            right: self.right - other.right,
            bottom: self.bottom - other.bottom,
            left: self.left - other.left,
        }
    }
}

impl Sum for TRBL {
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = Self>,
    {
        let mut total = Self::default();
        for i in iter {
            total += i
        }
        total
    }
}

impl SubAssign for TRBL {
    fn sub_assign(&mut self, other: Self) {
        *self = Self {
            top: self.top - other.top,
            right: self.right - other.right,
            bottom: self.bottom - other.bottom,
            left: self.left - other.left,
        };
    }
}
