use crate::layout::LayoutInfo;

/// A struct representing horizontal/vertical alignment
#[derive(Debug, Clone, Copy, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Alignment {
    /// The object should be aligned at the begining of the axis
    Begin,
    /// The object should be centered on the axis
    Center,
    /// The object should be at the end of the axis.
    End,
    /// The object should take us much space as possible on the axis
    Expand,
}

impl Alignment {
    /// Aligns a given axis
    pub const fn align(
        &self,
        mut outer: LayoutInfo,
        size: usize,
    ) -> LayoutInfo {
        match self {
            Alignment::Expand => outer,
            Alignment::End => {
                outer.start = outer.end.saturating_sub(size);
                outer
            }
            Alignment::Begin => {
                outer.end = outer.start + size;
                outer
            }
            Alignment::Center => {
                let centre = (outer.start + outer.end) / 2;
                LayoutInfo {
                    start: centre.saturating_sub(size / 2),
                    end: centre + (size / 2) + (size % 2),
                }
            }
        }
    }
}
