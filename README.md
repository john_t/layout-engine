# Layout Engine

Layout engine is a small rust project aimed to mimic css flexbox and
css grid. It also implements height-for-width and width-for-height
layout management.

## Features

 - `serde` - enables serde support on all structs and enums.
